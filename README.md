
# 🌐 [Website](https://shadoww.pages.dev)

My personal website, now hosted at: [https://shadoww.pages.dev](https://shadoww.pages.dev)


## 🔗 Links

[![xSha](https://img.shields.io/badge/xSha-Gitlab-orange?labelColor=white&style=flat&logo=gitlab&link=https://gitlab.com/xSha)](https://gitlab.com/xSha)
[![0xSha](https://img.shields.io/badge/0xSha-YouTube-red?labelColor=white&style=flat&logo=YouTube&logoColor=red&link=https://www.youtube.com/channel/UCHqasaDnsOJ8C3qgp0zK8_w)](https://www.youtube.com/channel/UCHqasaDnsOJ8C3qgp0zK8_w)
## 📖 [License](LICENSE)

[MIT](https://choosealicense.com/licenses/mit/)

